# Simple Input Simulator
[English](#English) [中文](#Chinese)

#### English
#### What can it do
+ Record simple mouse and keyboard event
+ Save the events as script
+ Replay the script to reproduce the operation recorded
### Gramma
#### Mouse Control
+ Mouse Move
  ``` 
  Move to X Y after Z
  ```
+ Mouse Press
  ```
  Press (Primary, Secondary, Middle) after Z
  ```
+ Mouse Release
  ```
  Release (Primary, Secondary, Middle) after Z
  ```
+ Mouse wheel
  ```
  Scroll up after Z
  Scroll down after Z
  ```
#### Key Control
+ Key Press
  ```
  Press (KeyName) after Z
  ```
+ Key Release
  ```
  Release (KeyName) after Z
  ```

### Reference
1.[KeymouseGo](https://github.com/taojy123/KeymouseGo)
The idea of making this repository was inspired by the idea of KeymouseGo, which is an opensource tool like Key Mouse Genie. You may find that the implementation looks alike KeymouseGo in some aspect.

2.[Crafting Interprters](https://github.com/munificent/craftinginterpreters)

3.[Crafting Interprters zh](https://github.com/GuoYaxiang/craftinginterpreters_zh)
Although the "Interpreter" in my project is not a real interpreter, I refered some of the idea in this book to parse the script. 

#### PS

If this repository is infringing, please notice me in issue or email me so I can make up for it, or delete this repository.

#### Chinese
#### 它能做什么
+ 记录简单的鼠标与键盘操作
+ 将操作保存为脚本
+ 通过脚本重现记录的操作
### 语法
#### 鼠标控制
+ 鼠标移动
  ``` 
  经过 Z 移动到 X Y
  ```
+ 鼠标按下
  ```
  经过 Z 按住 (主要功能键, 次要功能键, 中键) 
  ```
+ 鼠标释放
  ```
  经过 Z 释放 (主要功能键, 次要功能键, 中键)
  ```
+ 鼠标滚轮
  ```
  经过 Z 上滚
  经过 Z 下滚
  ```
#### 键盘控制
+ 按下键
  ```
  经过 Z 按住 (键名)
  ```
+ 释放键
  ```
  经过 Z 释放 (键名)
  ```

### 参考项目
1.[KeymouseGo](https://github.com/taojy123/KeymouseGo)
KeymouseGo是一款开源的类按键精灵软件,这个仓库是受到KeymouseGo的启发而创建的,因此实现功能的过程相似。(也可以看成是Java版的KeymouseGo?)

2.[Crafting Interprters](https://github.com/munificent/craftinginterpreters)

3.[Crafting Interprters zh](https://github.com/GuoYaxiang/craftinginterpreters_zh)
我写的所谓”解释器“其实并不是真正的解释器，因为恰好在看这本书，就借用了里面的一些思想来实现脚本的解释过程。(类比一下项目1就知道我的实现过程其实过于繁琐)

#### PS
如果你认为这个Repository侵权了，请通过issue或邮件告知，我会改掉相关部分或是删库。