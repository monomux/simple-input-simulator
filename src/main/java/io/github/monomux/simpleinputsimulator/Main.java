package io.github.monomux.simpleinputsimulator;

import io.github.monomux.simpleinputsimulator.Controller.MainWndController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class Main extends Application {

    private MainWndController controller;

    @Override
    public void stop() throws Exception {
        String lang = controller.getLang();
        File file = new File("Config.xml");
        if(!file.exists()){
            try {
                file.createNewFile();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
                writer.write(String.join("", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n",
                                "<Config>\n", "    <Language>+" + lang + "</Language>\n", "</Config>"));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(file);
            document.getElementsByTagName("Language").item(0).getFirstChild().setNodeValue(lang);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.transform(new DOMSource(document), new StreamResult("Config.xml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        super.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
//        System.setProperty("prism.allowhidpi", "false");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainWnd.fxml"));
        primaryStage.setScene(new Scene(loader.load()));
        controller = loader.getController();
        primaryStage.setTitle("Simple Input Simulator");
        Image image = new Image(getClass().getResourceAsStream("/Icon/Icons8-Windows-8-Programming-Java-Duke-Logo.png"));
        primaryStage.getIcons().add(image);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void launchApp(String[] args){
        launch(args);
    }
}
