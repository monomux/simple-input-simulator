package io.github.monomux.simpleinputsimulator.FakeLexer.Event;

import io.github.monomux.simpleinputsimulator.FakeLexer.TokenType;
import javafx.scene.input.MouseButton;
import javafx.scene.robot.Robot;
import javafx.stage.Screen;


public class MouseEvent implements InputEvent{
    private final long delay;
    private double positionX, positionY;
    private MouseButton button;
    private int wheelAmt;
    private final TokenType type;
    private final Robot robot;
    private final double scaleX = Screen.getPrimary().getOutputScaleX();
    private final double scaleY = Screen.getPrimary().getOutputScaleY();

    public MouseEvent(Robot robot, TokenType type, long delay, Object... parameters) {
        this.robot = robot;
        this.type = type;
        this.delay = delay;
        switch (type){
            case MOVE:positionX = (double)parameters[0];positionY = (double)parameters[1];break;
            case PRESS:
            case RELEASE:button = (MouseButton) parameters[0];break;
            case SCROLL:
                switch ((String) parameters[0]){
                    case "UP":wheelAmt = -120;break;
                    case "DOWN":wheelAmt = 120;break;
                }
        }
    }

    @Override
    public void executeEvent() throws InterruptedException {
        Thread.sleep(delay/1000000);

        switch (type){
            case MOVE:robot.mouseMove(positionX / scaleX, positionY / scaleY);break;
            case PRESS:robot.mousePress(MouseButton.MIDDLE);break;
            case RELEASE:robot.mouseRelease(MouseButton.MIDDLE);break;
            case SCROLL: robot.mouseWheel(wheelAmt);break;
        }
    }
}
