package io.github.monomux.simpleinputsimulator.Listener;

import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent;
import com.github.kwhat.jnativehook.keyboard.NativeKeyListener;
import com.github.kwhat.jnativehook.mouse.NativeMouseEvent;
import com.github.kwhat.jnativehook.mouse.NativeMouseInputListener;
import com.github.kwhat.jnativehook.mouse.NativeMouseWheelEvent;
import com.github.kwhat.jnativehook.mouse.NativeMouseWheelListener;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class SimpleListener implements NativeMouseInputListener, NativeMouseWheelListener, NativeKeyListener {
    BufferedWriter writer;
    String lang;
    String[] btnName = {"Primary", "Secondary", "Middle"};
    String[] btnNameCN = {"主要功能键", "次要功能键", "中键"};
    LinkedList<String> list = new LinkedList<>();

    long lastEvent;

    public void setLang(String lang){
        this.lang = lang;
    }

    public void setWriter(BufferedWriter writer){
        this.writer = writer;
    }

    public void setLastEvent(long lastEvent){
        this.lastEvent = lastEvent;
    }

    public void saveScript() throws IOException {
        list.pollLast();
        list.pop();
        for(String str:list){
            writer.write(str);
        }
        writer.close();
    }

    @Override
    public void nativeMouseWheelMoved(NativeMouseWheelEvent nativeEvent) {
        System.out.println("Mosue Wheel Moved: " + nativeEvent.getWheelRotation());
        switch (lang){
            case "en":
                list.add(String.join(" ", "Roll", nativeEvent.getWheelRotation()>0?"Down":"Up",
                        "After", String.valueOf(System.nanoTime()-lastEvent), "\n"));break;
            case "zh-cn":
                list.add(String.join(" ", "经过", String.valueOf(System.nanoTime()-lastEvent),
                        nativeEvent.getWheelRotation()>0?"下滚":"上滚", "\n"));break;
        }
        lastEvent = System.nanoTime();
    }

    public void nativeMousePressed(NativeMouseEvent nativeEvent) {
        System.out.println("Mouse Pressed: " + nativeEvent.getButton());
        switch (lang){
            case "en":
                list.add(String.join(" ", "Press", btnName[nativeEvent.getButton()-1],
                        "After", String.valueOf(System.nanoTime()-lastEvent), "\n"));break;
            case "zh-cn":
                list.add(String.join(" ", "经过", String.valueOf(System.nanoTime()-lastEvent),
                        "按住", btnNameCN[nativeEvent.getButton()-1], "\n"));break;
        }
        lastEvent = System.nanoTime();
    }

    public void nativeMouseReleased(NativeMouseEvent nativeEvent) {
        System.out.println("Mouse Released: " + nativeEvent.getButton());
        try {
            switch (lang){
                case "en":
                    writer.write(String.join(" ", "Release", btnName[nativeEvent.getButton()-1],
                            "After", String.valueOf(System.nanoTime()-lastEvent), "\n"));break;
                case "zh-cn":
                    writer.write(String.join(" ", "经过", String.valueOf(System.nanoTime()-lastEvent),
                            "释放", btnNameCN[nativeEvent.getButton()-1], "\n"));break;
            }
            lastEvent = System.nanoTime();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void nativeMouseMoved(NativeMouseEvent nativeEvent) {
        System.out.println("Mouse Moved: " + nativeEvent.getX() + ", " + nativeEvent.getY());
        switch (lang){
            case "en":
                list.add(String.join(" ", "Move to", String.valueOf(nativeEvent.getX()), String.valueOf(nativeEvent.getY()),
                        "After", String.valueOf(System.nanoTime()-lastEvent), "\n"));break;
            case "zh-cn":
                list.add(String.join(" ", "经过", String.valueOf(System.nanoTime()-lastEvent),
                        "移动到", String.valueOf(nativeEvent.getX()), String.valueOf(nativeEvent.getY()), "\n"));break;
        }
        lastEvent = System.nanoTime();
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeEvent) {
        System.out.println("Key Pressed: " + KeyCodeMapper.getKeyText(nativeEvent.getKeyCode()) + " Code=\'" + nativeEvent.getKeyCode() + "\'");
        String keyName = KeyCodeMapper.getKeyText(nativeEvent.getKeyCode());
        if(!keyName.contains("Unknown")) {
            switch (lang) {
                case "en":
                    list.add(String.join(" ", "Press", keyName,
                            "After", String.valueOf(System.nanoTime() - lastEvent), "\n"));
                    break;
                case "zh-cn":
                    list.add(String.join(" ", "经过", String.valueOf(System.nanoTime() - lastEvent),
                            "按住", keyName, "\n"));
                    break;
            }
            lastEvent = System.nanoTime();
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeEvent) {
        System.out.println("Key Released: " + KeyCodeMapper.getKeyText(nativeEvent.getKeyCode()));
        String keyName = KeyCodeMapper.getKeyText(nativeEvent.getKeyCode());
        if(!keyName.contains("Unknown")) {
            switch (lang) {
                case "en":
                    list.add(String.join(" ", "Release", keyName,
                            "After", String.valueOf(System.nanoTime() - lastEvent), "\n"));
                    break;
                case "zh-cn":
                    list.add(String.join(" ", "经过", String.valueOf(System.nanoTime() - lastEvent),
                            "释放", keyName, "\n"));
                    break;
            }
            lastEvent = System.nanoTime();
        }
    }
}
