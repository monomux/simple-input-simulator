package io.github.monomux.simpleinputsimulator.FakeLexer;

import io.github.monomux.simpleinputsimulator.FakeLexer.Event.InputEvent;
import io.github.monomux.simpleinputsimulator.FakeLexer.Event.KeyEvent;
import io.github.monomux.simpleinputsimulator.FakeLexer.Event.MouseEvent;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseButton;
import javafx.scene.robot.Robot;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Interpreter {
    private final List<Token> tokens;
    private final List<InputEvent> events = new LinkedList<>();
    private int currentLine;
    private final Robot robot;

    public Interpreter(Robot robot, List<Token> tokens){
        this.robot = robot;
        this.tokens = tokens;
    }

    private void parse(){
        currentLine = 1;
        ListIterator<Token> iterator = tokens.listIterator();
        Token token;
        InputEvent event = null;
        long delay = 0;
        double posX = 0, posY = 0;
        TokenType type = null;
        MouseButton button = null;
        String name = null;
        boolean isMouse = false;
        Pattern pattern = Pattern.compile("PRIMARY|SECONDARY|MIDDLE");
        Matcher matcher;
        while(iterator.hasNext()){
            token = iterator.next();
            if(currentLine != token.line){
                currentLine++;
                if(isMouse){
                    switch (type){
                        case SCROLL: event = new MouseEvent(robot, type, delay, (Object) name);break;
                        case PRESS:
                        case RELEASE:event = new MouseEvent(robot, type, delay, (Object) button);break;
                        case MOVE:event = new MouseEvent(robot, type, delay, (Object) posX, (Object) posY);break;
                    }
                }else{
                    event = new KeyEvent(robot, type, delay, name);
                }
                events.add(event);
                isMouse = false;
            }
            switch (token.type){
                case MOVE:type = token.type;isMouse = true;break;
                case TO:
                    posX = Double.parseDouble(iterator.next().lexeme);
                    posY = Double.parseDouble(iterator.next().lexeme);break;
                case AFTER:delay = Long.parseLong(iterator.next().lexeme);break;
                case PRESS:
                case RELEASE:
                    type = token.type;
                    token = iterator.next();
                    matcher = pattern.matcher(token.lexeme);
                    if(matcher.find()){
                        switch (token.lexeme){
                            case "PRIMARY": button = MouseButton.PRIMARY;break;
                            case "SECONDARY": button = MouseButton.SECONDARY;break;
                            case "MIDDLE": button = MouseButton.MIDDLE;break;
                        }
                        isMouse = true;
                    }else{
                        name = token.lexeme;
                        isMouse = false;
                    }break;
                case SCROLL:
                    isMouse = true;
                    type = token.type;
                    token = iterator.next();
                    switch (token.lexeme){
                        case "UP":
                        case "DOWN":name = token.lexeme;break;
                    }
                case EOF:
            }
        }
    }

    public void execute() throws InterruptedException {
        parse();
        for(InputEvent event:events){
            event.executeEvent();
        }
    }

    public void repeat() throws InterruptedException {
        for(InputEvent event:events){
            event.executeEvent();
        }
    }

    public void execute(ProgressBar progressBar) throws InterruptedException {
        parse();
        repeat(progressBar);
    }

    public void repeat(ProgressBar progressBar) throws InterruptedException {
        int len = events.size();
        double index = 0;
        for(InputEvent event:events){
            event.executeEvent();
            progressBar.setProgress(index/len);
            index++;
        }
        progressBar.setProgress(1);
    }
}
