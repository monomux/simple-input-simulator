package io.github.monomux.simpleinputsimulator.FakeLexer.Event;

public interface InputEvent {
    public void executeEvent() throws InterruptedException;
}
