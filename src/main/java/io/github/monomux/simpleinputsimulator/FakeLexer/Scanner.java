package io.github.monomux.simpleinputsimulator.FakeLexer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.github.monomux.simpleinputsimulator.FakeLexer.TokenType.*;

public class Scanner {
    private final List<Token> tokens = new LinkedList<>();
    private BufferedReader reader;

    private int currentline = 1;

    public Scanner(URL url){
        try {
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Scanner(String path){
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Token> getTokens(){
        StringTokenizer tokenizer = null;
        try{
            String string = reader.readLine();
            String strToken, oriToken;
            Pattern pattern = Pattern.compile("[0-9]+%*");
            Matcher matcher;
            while(string != null){
                tokenizer = new StringTokenizer(string);
                while(tokenizer.hasMoreTokens()){
                    oriToken = tokenizer.nextToken();
                    strToken = oriToken.toUpperCase();
                    switch (strToken){
                        case "MOVE": tokens.add(new Token(MOVE, strToken, currentline));break;
                        case "PRESS":
                        case "按住":
                            tokens.add(new Token(PRESS, "PRESS", currentline));break;
                        case "RELEASE":
                        case "松开":
                        case "释放":
                            tokens.add(new Token(RELEASE, "RELEASE", currentline));break;
                        case "SCROLL":tokens.add(new Token(SCROLL, strToken, currentline));break;
                        case "移动到":
                            tokens.add(new Token(MOVE, "MOVE", currentline));
                            tokens.add(new Token(TO, "TO", currentline));break;

                        case "TO":tokens.add(new Token(TO, strToken, currentline));break;
                        case "AFTER":
                        case "经过":
                            tokens.add(new Token(AFTER, "AFTER", currentline));break;

                        case "PRIMARY":
                        case "SECONDARY":
                        case "MIDDLE":
                        case "UP":
                        case "DOWN":
                            tokens.add(new Token(PARAMETER, strToken, currentline));break;
                        case "上滚":
                            tokens.add(new Token(SCROLL, "ROLL", currentline));
                            tokens.add(new Token(PARAMETER, "UP", currentline));break;
                        case "下滚":
                            tokens.add(new Token(SCROLL, "ROLL", currentline));
                            tokens.add(new Token(PARAMETER, "DOWN", currentline));break;
                        case "主要功能键":tokens.add(new Token(PARAMETER, "PRIMARY", currentline));break;
                        case "次要功能键":tokens.add(new Token(PARAMETER, "SECONDARY", currentline));break;
                        case "中键":tokens.add(new Token(PARAMETER, "MIDDLE", currentline));break;
                        default:
                            matcher = pattern.matcher(strToken);
                            if(matcher.find()){
                                tokens.add(new Token(NUMBER, strToken, currentline));
                            }else{
                                tokens.add(new Token(PARAMETER, oriToken, currentline));
                            }
                    }
                }
                string = reader.readLine();
                currentline++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            tokens.add(new Token(EOF, null, currentline));
        }

        return tokens;
    }
}
