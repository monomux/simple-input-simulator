package io.github.monomux.simpleinputsimulator.Listener;

import java.util.ResourceBundle;

import static com.github.kwhat.jnativehook.keyboard.NativeKeyEvent.*;

public class KeyCodeMapper {

    static ResourceBundle bundle = ResourceBundle.getBundle("awt");
    
    // Edited Codes from NativeKeyEvent
    public static String getKeyText(int keyCode) {
        // Lookup text values.
        switch (keyCode) {
            case VC_ESCAPE:
                return bundle.getString("AWT.escape");

            // Begin Function Keys
            case VC_F1:
                return bundle.getString("AWT.f1");
            case VC_F2:
                return bundle.getString("AWT.f2");
            case VC_F3:
                return bundle.getString("AWT.f3");
            case VC_F4:
                return bundle.getString("AWT.f4");
            case VC_F5:
                return bundle.getString("AWT.f5");
            case VC_F6:
                return bundle.getString("AWT.f6");
            case VC_F7:
                return bundle.getString("AWT.f7");
            case VC_F8:
                return bundle.getString("AWT.f8");
            case VC_F9:
                return bundle.getString("AWT.f9");
            case VC_F10:
                return bundle.getString("AWT.f10");
            case VC_F11:
                return bundle.getString("AWT.f11");
            case VC_F12:
                return bundle.getString("AWT.f12");

            case VC_F13:
                return bundle.getString("AWT.f13");
            case VC_F14:
                return bundle.getString("AWT.f14");
            case VC_F15:
                return bundle.getString("AWT.f15");
            case VC_F16:
                return bundle.getString("AWT.f16");
            case VC_F17:
                return bundle.getString("AWT.f17");
            case VC_F18:
                return bundle.getString("AWT.f18");
            case VC_F19:
                return bundle.getString("AWT.f19");
            case VC_F20:
                return bundle.getString("AWT.f20");
            case VC_F21:
                return bundle.getString("AWT.f21");
            case VC_F22:
                return bundle.getString("AWT.f22");
            case VC_F23:
                return bundle.getString("AWT.f23");
            case VC_F24:
                return bundle.getString("AWT.f24");
            // End Function Keys

            // Begin Alphanumeric Zone
            case VC_BACKQUOTE:
                return bundle.getString("AWT.backQuote");

            case VC_1:
                return "1";
            case VC_2:
                return "2";
            case VC_3:
                return "3";
            case VC_4:
                return "4";
            case VC_5:
                return "5";
            case VC_6:
                return "6";
            case VC_7:
                return "7";
            case VC_8:
                return "8";
            case VC_9:
                return "9";
            case VC_0:
                return "0";

            case VC_MINUS:
                return bundle.getString("AWT.minus");
            case VC_EQUALS:
                return bundle.getString("AWT.equals");
            case VC_BACKSPACE:
                return bundle.getString("AWT.backSpace");

            case VC_TAB:
                return bundle.getString("AWT.tab");
            case VC_CAPS_LOCK:
                return bundle.getString("AWT.capsLock");

            case VC_A:
                return "A";
            case VC_B:
                return "B";
            case VC_C:
                return "C";
            case VC_D:
                return "D";
            case VC_E:
                return "E";
            case VC_F:
                return "F";
            case VC_G:
                return "G";
            case VC_H:
                return "H";
            case VC_I:
                return "I";
            case VC_J:
                return "J";
            case VC_K:
                return "K";
            case VC_L:
                return "L";
            case VC_M:
                return "M";
            case VC_N:
                return "N";
            case VC_O:
                return "O";
            case VC_P:
                return "P";
            case VC_Q:
                return "Q";
            case VC_R:
                return "R";
            case VC_S:
                return "S";
            case VC_T:
                return "T";
            case VC_U:
                return "U";
            case VC_V:
                return "V";
            case VC_W:
                return "W";
            case VC_X:
                return "X";
            case VC_Y:
                return "Y";
            case VC_Z:
                return "Z";

            case VC_OPEN_BRACKET:
                return bundle.getString("AWT.openBracket");
            case VC_CLOSE_BRACKET:
                return bundle.getString("AWT.closeBracket");
            case VC_BACK_SLASH:
                return bundle.getString("AWT.backSlash");

            case VC_SEMICOLON:
                return bundle.getString("AWT.semicolon");
            case VC_QUOTE:
                return bundle.getString("AWT.quote");
            case VC_ENTER:
                return bundle.getString("AWT.enter");

            case VC_COMMA:
                return bundle.getString("AWT.comma");
            case VC_PERIOD:
                return bundle.getString("AWT.period");
            case VC_SLASH:
                return bundle.getString("AWT.slash");

            case VC_SPACE:
                return bundle.getString("AWT.space");
            // End Alphanumeric Zone

            case VC_PRINTSCREEN:
                return bundle.getString("AWT.printScreen");
            case VC_SCROLL_LOCK:
                return bundle.getString("AWT.scrollLock");
            case VC_PAUSE:
                return bundle.getString("AWT.pause");

            // Begin Edit Key Zone
            case VC_INSERT:
                return bundle.getString("AWT.insert");
            case VC_DELETE:
                return bundle.getString("AWT.delete");
            case VC_HOME:
                return bundle.getString("AWT.home");
            case VC_END:
                return bundle.getString("AWT.end");
            case VC_PAGE_UP:
                return bundle.getString("AWT.pgup");
            case VC_PAGE_DOWN:
                return bundle.getString("AWT.pgdn");
            // End Edit Key Zone

            // Begin Cursor Key Zone
            case VC_UP:
                return bundle.getString("AWT.up");
            case VC_LEFT:
                return bundle.getString("AWT.left");
            case VC_CLEAR:
                return bundle.getString("AWT.clear");
            case VC_RIGHT:
                return bundle.getString("AWT.right");
            case VC_DOWN:
                return bundle.getString("AWT.down");
            // End Cursor Key Zone

            // Begin Numeric Zone
            case VC_NUM_LOCK:
                return bundle.getString("AWT.numLock");
            case VC_SEPARATOR:
                return bundle.getString("AWT.separator");
            // End Numeric Zone

            // Begin Modifier and Control Keys
            case VC_SHIFT:
                return bundle.getString("AWT.shift");
            case VC_CONTROL:
                return bundle.getString("AWT.control");
            case VC_ALT:
                return bundle.getString("AWT.alt");
            case VC_META:
                return bundle.getString("AWT.meta");
            case VC_CONTEXT_MENU:
                return bundle.getString("AWT.context");
            // End Modifier and Control Keys

            // Begin Media Control Keys
            case VC_POWER:
                return bundle.getString("AWT.power");
            case VC_SLEEP:
                return bundle.getString("AWT.sleep");
            case VC_WAKE:
                return bundle.getString("AWT.wake");

            case VC_MEDIA_PLAY:
                return bundle.getString("AWT.play");
            case VC_MEDIA_STOP:
                return bundle.getString("AWT.stop");
            case VC_MEDIA_PREVIOUS:
                return bundle.getString("AWT.previous");
            case VC_MEDIA_NEXT:
                return bundle.getString("AWT.next");
            case VC_MEDIA_SELECT:
                return bundle.getString("AWT.select");
            case VC_MEDIA_EJECT:
                return bundle.getString("AWT.eject");

            case VC_VOLUME_MUTE:
                return bundle.getString("AWT.mute");
            case VC_VOLUME_UP:
                return bundle.getString("AWT.volup");
            case VC_VOLUME_DOWN:
                return bundle.getString("AWT.voldn");

            case VC_APP_MAIL:
                return bundle.getString("AWT.app_mail");
            case VC_APP_CALCULATOR:
                return bundle.getString("AWT.app_calculator");
            case VC_APP_MUSIC:
                return bundle.getString("AWT.app_music");
            case VC_APP_PICTURES:
                return bundle.getString("AWT.app_pictures");

            case VC_BROWSER_SEARCH:
                return bundle.getString("AWT.search");
            case VC_BROWSER_HOME:
                return bundle.getString("AWT.homepage");
            case VC_BROWSER_BACK:
                return bundle.getString("AWT.back");
            case VC_BROWSER_FORWARD:
                return bundle.getString("AWT.forward");
            case VC_BROWSER_STOP:
                return bundle.getString("AWT.stop");
            case VC_BROWSER_REFRESH:
                return bundle.getString("AWT.refresh");
            case VC_BROWSER_FAVORITES:
                return bundle.getString("AWT.favorites");
            // End Media Control Keys

            // Begin Japanese Language Keys
            case VC_KATAKANA:
                return bundle.getString("AWT.katakana");
            case VC_UNDERSCORE:
                return bundle.getString("AWT.underscore");
            case VC_FURIGANA:
                return bundle.getString("AWT.furigana");
            case VC_KANJI:
                return bundle.getString("AWT.kanji");
            case VC_HIRAGANA:
                return bundle.getString("AWT.hiragana");
            case VC_YEN:
                return bundle.getString("AWT.yen");
            // End Japanese Language Keys

            // Begin Sun keyboards
            case VC_SUN_HELP:
                return bundle.getString("AWT.sun_help");

            case VC_SUN_STOP:
                return bundle.getString("AWT.sun_stop");
            case VC_SUN_PROPS:
                return bundle.getString("AWT.sun_props");
            case VC_SUN_FRONT:
                return bundle.getString("AWT.sun_front");
            case VC_SUN_OPEN:
                return bundle.getString("AWT.sun_open");
            case VC_SUN_FIND:
                return bundle.getString("AWT.sun_find");
            case VC_SUN_AGAIN:
                return bundle.getString("AWT.sun_again");
            case VC_SUN_UNDO:
                return bundle.getString("AWT.sun_undo");
            case VC_SUN_COPY:
                return bundle.getString("AWT.sun_copy");
            case VC_SUN_INSERT:
                return bundle.getString("AWT.sun_insert");
            case VC_SUN_CUT:
                return bundle.getString("AWT.sun_cut");
            // End Sun keyboards

            case VC_UNDEFINED:
                return bundle.getString("AWT.undefined");
        }

        return bundle.getString("AWT.unknown") +
                " keyCode: 0x" + Integer.toString(keyCode, 16);
    }
}
