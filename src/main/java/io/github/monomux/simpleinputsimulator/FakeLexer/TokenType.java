package io.github.monomux.simpleinputsimulator.FakeLexer;

public enum TokenType {
    MOVE, PRESS, RELEASE, SCROLL,
    PARAMETER, NUMBER, TO, AFTER,
    EOF
}
