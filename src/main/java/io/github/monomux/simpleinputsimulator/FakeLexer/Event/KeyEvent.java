package io.github.monomux.simpleinputsimulator.FakeLexer.Event;

import io.github.monomux.simpleinputsimulator.FakeLexer.TokenType;
import javafx.scene.input.KeyCode;
import javafx.scene.robot.Robot;

public class KeyEvent implements InputEvent{
    private final long delay;
    private String keyName;
    private final TokenType type;
    private final Robot robot;

    public KeyEvent(Robot robot, TokenType type, long delay, String keyName) {
        this.robot = robot;
        this.type = type;
        this.delay = delay;
        this.keyName = keyName;
    }
    @Override
    public void executeEvent() throws InterruptedException {
        Thread.sleep(delay/1000000);
        switch (type){
            case PRESS:robot.keyPress(KeyCode.getKeyCode(keyName));break;
            case RELEASE:robot.keyRelease(KeyCode.getKeyCode(keyName));break;
        }
    }
}
