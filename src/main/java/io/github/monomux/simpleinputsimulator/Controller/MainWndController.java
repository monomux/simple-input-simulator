package io.github.monomux.simpleinputsimulator.Controller;

import com.github.kwhat.jnativehook.GlobalScreen;
import com.github.kwhat.jnativehook.NativeHookException;
import com.github.kwhat.jnativehook.dispatcher.SwingDispatchService;
import io.github.monomux.simpleinputsimulator.FakeLexer.Interpreter;
import io.github.monomux.simpleinputsimulator.FakeLexer.Scanner;
import io.github.monomux.simpleinputsimulator.FakeLexer.Token;
import io.github.monomux.simpleinputsimulator.Listener.SimpleListener;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.robot.Robot;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainWndController {
    @FXML
    private Label identifier;

    @FXML
    private Button executeBtn;

    @FXML
    private Label languageLabel;

    @FXML
    private ChoiceBox<String> scriptBox;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Label scriptLabel;

    @FXML
    private ChoiceBox<String> languageBox;

    @FXML
    private Button recordBtn;

    private SimpleListener listener;

    String record, stopRecord;
    String waiting, scanning, executing;
    final String[] choices = {"en", "zh-cn"};

    boolean isRecording;
    private String latestScript = "";

    private Robot robot;
    private Scanner scanner;
    private Interpreter interpreter;
    private Task<Void> exeTask;

    private File scriptFile;
    private File lastestSave;

    private BufferedReader reader;
    private BufferedWriter writer;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

    public String getLang(){
        return languageBox.getValue();
    }

    public void initialize(){
        GlobalScreen.setEventDispatcher(new SwingDispatchService());
        listener = new SimpleListener();
        GlobalScreen.addNativeKeyListener(listener);
        GlobalScreen.addNativeMouseListener(listener);
        GlobalScreen.addNativeMouseMotionListener(listener);
        GlobalScreen.addNativeMouseWheelListener(listener);

        robot = new Robot();

        recordBtn.setOnMouseClicked(event -> {
            if(isRecording){
                recordBtn.setText(record);
                languageBox.setDisable(false);
                executeBtn.setDisable(false);
                try{
                    listener.saveScript();
                    updateScriptList(scriptFile);
                    GlobalScreen.unregisterNativeHook();
                }catch (NativeHookException e){
                    System.err.println("There was a problem unregistering the native hook.");
                    System.err.println(e.getMessage());
                    Platform.exit();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                recordBtn.setText(stopRecord);
                languageBox.setDisable(true);
                executeBtn.setDisable(true);
                Date date = new Date();
                String filePath = String.join("",  System.getProperty("user.dir"), "/Script/",format.format(date), ".sis");
                lastestSave = new File(filePath);
                try{
                    lastestSave.createNewFile();
                    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(lastestSave)));
                    listener.setWriter(writer);
                    listener.setLang(languageBox.getValue());
                    listener.setLastEvent(System.nanoTime());
                    GlobalScreen.registerNativeHook();
                }catch (NativeHookException e){
                    System.err.println("There was a problem registering the native hook.");
                    System.err.println(e.getMessage());
                    Platform.exit();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            isRecording = !isRecording;
        });

        executeBtn.setOnMouseClicked(event -> {
            languageBox.setDisable(true);
            executeBtn.setDisable(true);
            recordBtn.setDisable(true);
            exeTask = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    if(!scriptBox.getItems().isEmpty()){
                        updateProgress(0, 100);
                        if(scriptBox.getValue().equals(latestScript)){
                            try {
                                interpreter.repeat();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }else{
                            String path = String.join("", "Script/", scriptBox.getValue());
                            identifier.setText(scanning);
                            scanner = new Scanner(path);
                            List<Token> list = scanner.getTokens();
//                            for(Token token:list){
//                                System.out.println(token);
//                            }
                            identifier.setText(executing);
                            interpreter = new Interpreter(robot, list);
                            try {
                                interpreter.execute(progressBar);
                            }catch (InterruptedException e){
                                e.printStackTrace();
                            }
                        }
                    }
                    Thread.sleep(500);
                    identifier.setText(waiting);
                    languageBox.setDisable(false);
                    executeBtn.setDisable(false);
                    recordBtn.setDisable(false);
                    return null;
                }
            };
//            progressBar.progressProperty().bind(exeTask.progressProperty());
            Platform.runLater(exeTask);
        });

        scriptFile = new File("Script");
        if(!scriptFile.exists()){
            scriptFile.mkdir();
        }else{
            updateScriptList(scriptFile);
            scriptBox.getSelectionModel().select(0);
        }
        scriptBox.getSelectionModel().selectedIndexProperty().addListener(((observable, oldValue, newValue) -> {
            scriptBox.getSelectionModel().select(newValue.intValue());
        }));

        languageBox.getItems().addAll(FXCollections.observableArrayList(choices));
        languageBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            languageBox.setValue(choices[newValue.intValue()]);
            setLanguage(String.join("", "/Language/", choices[newValue.intValue()], ".xml"));
        });
        File file = new File("Config.xml");
        if(!file.exists()){
            try {
                file.createNewFile();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
                writer.write(String.join("", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n",
                        "<Config>\n", "    <Language>en</Language>\n", "</Config>"));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(file);
            String lang = document.getElementsByTagName("Language").item(0).getFirstChild().getNodeValue().toLowerCase();
            languageBox.setValue(lang);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateScriptList(File scriptFolderFile){
        scriptBox.getItems().clear();
        Pattern pattern = Pattern.compile(".+\\.sis");
        Matcher matcher;
        for(String str:scriptFolderFile.list()){
//            System.out.println(str);
            matcher = pattern.matcher(str);
            if(matcher.find()){
                scriptBox.getItems().add(str);
            }
        }
    }

    public void setLanguage(String xmlPath){
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(getClass().getResourceAsStream(xmlPath));
            record = document.getElementsByTagName("RecordBtnStart").item(0).getFirstChild().getNodeValue();
            stopRecord = document.getElementsByTagName("RecordBtnStop").item(0).getFirstChild().getNodeValue();
            scriptLabel.setText(document.getElementsByTagName("ScriptLabel").item(0).getFirstChild().getNodeValue());
            languageLabel.setText(document.getElementsByTagName("LanguageLabel").item(0).getFirstChild().getNodeValue());
            executeBtn.setText(document.getElementsByTagName("ExecuteBtn").item(0).getFirstChild().getNodeValue());
            recordBtn.setText(document.getElementsByTagName("RecordBtnStart").item(0).getFirstChild().getNodeValue());
            waiting = document.getElementsByTagName("Waiting").item(0).getFirstChild().getNodeValue();
            scanning = document.getElementsByTagName("Scanning").item(0).getFirstChild().getNodeValue();
            executing = document.getElementsByTagName("Executing").item(0).getFirstChild().getNodeValue();
            identifier.setText(waiting);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
